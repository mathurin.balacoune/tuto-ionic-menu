import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind);

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
export class UsersProvider {

  db: any;
  remote: any;

  constructor() {

    this.db = new PouchDB('users');
    this.remote = 'http://localhost:5984/users';

    let options = {
      live: true,
      retry: true,
      continuous: true
    };

    this.db.sync(this.remote, options);
  }

  // get all users
  getAllUsers(_profil) {
    let params = {
      profil:  _profil
    };
    return this.db.find({selector: params});
  }

  // get user bu login and password
  getUserByLoginAndPassword(_login, _password) {
    let params = {
      login:  _login,
      password: _password
    };
    return this.db.find({selector: params});
  }

  // create a new user
  createUser(user) {
    return this.db.post(user);
  }

  // update user
  updateUser(user) {
    return this.db.put(user);
  }

  // delete user
  deleteUser(user) {
    return this.db.remove(user);
  }
}
