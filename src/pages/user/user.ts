import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdminPage } from "../admin/admin";
import { TailleurPage } from "../tailleur/tailleur";
import { ClientPage } from "../client/client";

/**
 * Generated class for the UserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  constructor(private navCtrl: NavController) {
  }

  showPage(page) {

    if(page == 'admin') {
      this.navCtrl.push(AdminPage);
    }

    if(page == 'client') {
      this.navCtrl.push(ClientPage);
    }

    if(page == 'tailleur') {
      this.navCtrl.push(TailleurPage);
    }
  }

}
