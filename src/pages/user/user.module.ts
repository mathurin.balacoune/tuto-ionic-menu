import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPage } from './user';
import {AdminPageModule} from "../admin/admin.module";
import {ClientPageModule} from "../client/client.module";
import {TailleurPageModule} from "../tailleur/tailleur.module";

@NgModule({
  declarations: [
    UserPage
  ],
  imports: [
    IonicPageModule.forChild(UserPage),
    AdminPageModule,
    ClientPageModule,
    TailleurPageModule
  ],
  exports: [
    UserPage
  ]
})
export class UserPageModule {}
