import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TailleurPage } from './tailleur';

@NgModule({
  declarations: [
    TailleurPage,
  ],
  imports: [
    IonicPageModule.forChild(TailleurPage),
  ],
  exports: [
    TailleurPage
  ]
})
export class TailleurPageModule {}
