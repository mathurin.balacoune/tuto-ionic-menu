import { Component } from '@angular/core';
import { AlertController, NavController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { MyApp } from "../../app/app.component";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private navCtrl: NavController, private alertCtrl: AlertController, private storage: Storage) {
  }

  // deconnexion
  deconnexion() {
    let confirm = this.alertCtrl.create({
      title: 'Deconnexion',
      message: 'Do you really want to deconnexion ?',
      buttons: [
        {
          text: 'Annuler',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.storage.remove('user').then(() => {
              this.navCtrl.setRoot(MyApp);
            });
          }
        }
      ]
    });
    confirm.present();
  }

}
