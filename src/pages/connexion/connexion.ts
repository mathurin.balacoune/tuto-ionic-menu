import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from "../../app/app.component";
import { UsersProvider } from "../../providers/users/users";
import {Validators, FormBuilder, FormGroup} from "@angular/forms";

/**
 * Generated class for the ConnexionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-connexion',
  templateUrl: 'connexion.html',
})
export class ConnexionPage {

  connexionForm: FormGroup;

  constructor(private navCtrl: NavController, private storage: Storage, private usersProvider: UsersProvider, private alertCtrl: AlertController, private formBuilder: FormBuilder) {
    this.connexionForm = formBuilder.group({
      login: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  connexion() {
    let user = null;
    this.usersProvider.getUserByLoginAndPassword(this.connexionForm.value.login,this.connexionForm.value.password).then((response) => {
      user = response.docs[0];
      if(user) {
        this.storage.set('user', user);
        this.navCtrl.setRoot(MyApp);
        this.connexionForm.value.login = '';
      }else {
        this.showErreurConnexion();
      }
      this.connexionForm.value.password = '';
    });
  }

  showErreurConnexion() {
    let alert = this.alertCtrl.create({
      title:'Error',
      subTitle:'login ou password incorrect',
      buttons:['OK']
    });
    alert.present();
    return;
  }

}
