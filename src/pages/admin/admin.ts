import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { UsersProvider } from "../../providers/users/users";

/**
 * Generated class for the AdminPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  admins: any;

  constructor(private alertCtrl: AlertController, private usersProvider: UsersProvider) {
  }

  // load default
  ionViewDidLoad() {
    this.getAllAdmin();
  }

  // get all admin
  getAllAdmin() {
    this.usersProvider.getAllUsers('admin').then((response) => {
      this.admins = response.docs;
    });
  }

  // create admin
  createAdmin(admin) {
    let params = {
      prenom: admin.prenom,
      nom: admin.nom,
      phone: admin.phone,
      login: admin.prenom,
      password: 'passer',
      latitude: '0.0',
      longitude: '0.0',
      profil: 'admin'
    };
    this.usersProvider.createUser(params).then((response) => {
      this.getAllAdmin();
    });
  }

  // update admin
  updateAdmin(admin, data) {
    let params = {
      _id: admin._id,
      _rev: admin._rev,
      prenom: data.prenom,
      nom: data.nom,
      phone: data.phone,
      login: admin.prenom,
      password: admin.password,
      latitude: admin.latitude,
      longitude: admin.longitude,
      profil: admin.profil
    };
    this.usersProvider.updateUser(params).then((response) => {
      this.getAllAdmin();
    });
  }

  // delete admin
  deleteAdmin(admin) {
    this.usersProvider.deleteUser(admin).then((response) => {
      this.getAllAdmin();
    });
  }

  // dialog add admin
  create() {
    let prompt = this.alertCtrl.create({
      title: 'New Admin',
      inputs: [
        {
          name: 'prenom',
          placeholder: 'Prénom'
        },
        {
          name: 'nom',
          placeholder: 'Nom'
        },
        {
          name: 'phone',
          placeholder: 'Phone'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.createAdmin(data);
          }
        }
      ]
    });
    prompt.present();
  }

  // dialog update admin
  update(admin) {
    let prompt = this.alertCtrl.create({
      title: 'Update Admin',
      inputs: [
        {
          name: 'prenom',
          value: admin.prenom,
          placeholder: 'Prénom'
        },
        {
          name: 'nom',
          value: admin.nom,
          placeholder: 'Nom'
        },
        {
          name: 'phone',
          value: admin.phone,
          placeholder: 'Phone'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: data => {
            this.updateAdmin(admin,data);
          }
        }
      ]
    });
    prompt.present();
  }

  // dialog remove admin
  delete(admin) {
    let confirm = this.alertCtrl.create({
      title: 'Remove admin',
      message: 'Do you really want to remove l admin '+ admin.prenom + ' ' + admin.nom +' ?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remove',
          handler: () => {
            this.deleteAdmin(admin);
          }
        }
      ]
    });
    confirm.present();
  }

}
